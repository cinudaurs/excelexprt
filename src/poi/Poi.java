/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poi;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Pablo nebot
 */
public class Poi {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here

        JsonArray arr = leerJson("stdntrprt.json");

        ArrayList<StudentReport> menu=crearMenu(arr);
        generarExcell(menu);

    }
    public static void generarExcell( ArrayList<StudentReport> studentReport) throws FileNotFoundException, IOException{

        String subject = null;
        String attendance = null;

        Workbook wb = new HSSFWorkbook();

        Sheet sheet = wb.createSheet("menu");

        Row row = sheet.createRow(0);

        Cell cell0 = row.createCell(0);
        Cell cell1 = row.createCell(1);
        Cell cell2 = row.createCell(2);
        Cell cell3 = row.createCell(3);
        Cell cell4 = row.createCell(4);
        Cell cell5 = row.createCell(5);
        Cell cell6 = row.createCell(6);
        //      Cell cell7 = row.createCell(7);

        cell0.setCellValue("date");
        cell1.setCellValue("studentId");
        cell2.setCellValue("studentName");
        cell3.setCellValue("academicYear");
        cell4.setCellValue("semister");
        cell5.setCellValue("subjects");
        cell6.setCellValue("attendance");
        //     cell7.setCellValue("totalLectures");

        int subjectsLength = 0;

        //assumes the last element in the json array is the json element with just the key of totalLectures.
        int studentReportLength = studentReport.size()-1;

        for(int m=0;m < studentReportLength; m++){

            subjectsLength += studentReport.get(m).getSubjects().length;

        }

        int n = 0;
        int i = 1;

        while ( n < studentReportLength) {

            while (i < subjectsLength) {

                int j = studentReport.get(n).getSubjects().length;

                Row row1 = sheet.createRow(i);
                Cell ca = row1.createCell(0);
                ca.setCellValue(studentReport.get(n).getDate());

                Cell cb = row1.createCell(1);
                cb.setCellValue(studentReport.get(n).getStudentId());

                Cell cc = row1.createCell(2);
                cc.setCellValue(studentReport.get(n).getStudentName());

                Cell cd = row1.createCell(3);
                cd.setCellValue(studentReport.get(n).getAcademicYear());

                Cell ce = row1.createCell(4);
                ce.setCellValue(studentReport.get(n).getSemister());

                Cell cf = row1.createCell(5);
                cf.setCellValue(studentReport.get(n).getSubjects()[0]);

                Cell cg = row1.createCell(6);
                cg.setCellValue(studentReport.get(n).getAttendance()[0]);

                int k = 1;

                while (k < j) {

                    Row embedRowk = sheet.createRow(i + k);
                    Cell embedCellSubj = embedRowk.createCell(5);

                    Cell embedCellAtt = embedRowk.createCell(6);

                    embedCellSubj.setCellValue(studentReport.get(n).getSubjects()[k]);

                    embedCellAtt.setCellValue(studentReport.get(n).getAttendance()[k]);

                    k += 1;

                }

                k = k - 1;

                i += j;


                Row totalRow = sheet.createRow(i);
                Cell totalCell = totalRow.createCell(4);
                totalCell.setCellValue("Total Lectures");

                Cell totalCellVal = totalRow.createCell(6);
                totalCellVal.setCellValue(studentReport.get(n).getTotalLectures());

                i+=1;

            }


            n += 1;

        }


        if (studentReport.get(n).getFinalTotalLectures() != null ){

            String finalTotalLecturesVal = studentReport.get(n).getFinalTotalLectures();

            Row totalRow = sheet.createRow(i);
            Cell totalCell = totalRow.createCell(4);
            totalCell.setCellValue("Total Lectures");

            Cell totalCellVal = totalRow.createCell(6);
            totalCellVal.setCellValue(studentReport.get(n).getFinalTotalLectures());


        }




        sheet. autoSizeColumn(7);


        FileOutputStream fileOut = new FileOutputStream("productos.xls");
        wb.write(fileOut);
        System.out.println("excel generaado con exito");
        fileOut.close();

    }
    public static ArrayList<StudentReport> crearMenu(JsonArray arr){

        ArrayList<StudentReport> menu = new  ArrayList<StudentReport>();

        for (int i = 0; i < arr.size()-1; i++) {

            StudentReport studentReport = new StudentReport();

            JsonObject  json3 = (JsonObject) arr.get(i);

            JsonElement  dateElement = json3.get("date");

            String date = dateElement.getAsString();

            studentReport.setDate(date);

            JsonElement  studentIdElement = json3.get("studentId");

            String studentId = studentIdElement.getAsString();
            studentReport.setStudentId(studentId);

            JsonElement studentNameElement =json3.get("studentName");
            String studentName = studentNameElement.getAsString();
            studentReport.setStudentName(studentName);

            JsonElement academicYeareElement =json3.get("academicYear");
            String academicYear = academicYeareElement.getAsString();
            studentReport.setAcademicYear(academicYear);


            JsonElement semisterElement =json3.get("semister");
            String semister = semisterElement.getAsString();
            studentReport.setSemister(semister);


            JsonElement  subjectsElement = json3.get("subjects");
            JsonArray subjectsArray = subjectsElement.getAsJsonArray();
            String[] subjects = new String[subjectsArray.size()];
            for (int j = 0; j < subjectsArray.size(); j++){
                subjects[j]= subjectsArray.get(j).getAsString();
            }
            studentReport.setSubjects(subjects);


            JsonElement  attendanceElement = json3.get("attendance");
            JsonArray attendanceArray = attendanceElement.getAsJsonArray();
            String[] attendance = new String[attendanceArray.size()];
            for (int j = 0; j < attendanceArray.size(); j++){
                attendance[j]= attendanceArray.get(j).getAsString();
            }
            studentReport.setAttendance(attendance);


            JsonElement totalLecturesElement =json3.get("totalLectures");
            String totalLectures = totalLecturesElement.getAsString();
            studentReport.setTotalLectures(totalLectures);

            menu.add(studentReport);
        }

            JsonObject jsonfinal = (JsonObject) arr.get(arr.size()-1);
            JsonElement finalTotalLecturesElement = jsonfinal.get("totalLectures");
            String finalTotalLectures = finalTotalLecturesElement.getAsString();

            StudentReport studentReport = new StudentReport();
            studentReport.setFinalTotalLectures(finalTotalLectures);

            menu.add(studentReport);



        return menu;
    }
    public static JsonArray leerJson(String file) {

        JsonArray jsonArray = new JsonArray();

        try {
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(new FileReader(file));
            jsonArray = jsonElement.getAsJsonArray();
        } catch (FileNotFoundException e) {

        } catch (IOException ioe) {

        }
        return jsonArray;
    }
}
